package com.example.blog

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager

/**
 * @author Dominik Kiszka {dominikk19}
 * @project blog
 * @date 17.04.2020
 */
@DataJpaTest
class RepositoriesTests @Autowired constructor(
        val entityManager: TestEntityManager,
        val userRepository: UserRepository,
        val articleRepository: ArticleRepository
) {

    @Test
    fun `should return article when find by id `() {
        val userTom = User("tomB", "Tom", "Big", "form Poland")
        entityManager.persist(userTom)
        val article = Article("Spring application with Kotlin",
                "Spring community ...",
                "Lorem ipsum",
                userTom)
        entityManager.persist(article)

        entityManager.flush()
        val foundArticle = articleRepository.findById(article.id!!).orElse(null)

        assertThat(foundArticle).isNotNull
        assertThat(foundArticle.title).isEqualTo(article.title)
    }

    @Test
    fun `should retun user for login`(){
        val userTom = User("tomB", "Tom", "Big", "form Poland")
        entityManager.persistAndFlush(userTom)
        val foundUser = userRepository.findByLogin(userTom.login)?:throw NullPointerException()

        assertThat(foundUser).isNotNull
        assertThat(foundUser.login).isEqualTo(userTom.login)
    }


}
