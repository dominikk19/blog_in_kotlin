package com.example.blog

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.web.server.ResponseStatusException

/**
 * @author Dominik Kiszka {dominikk19}
 * @project blog
 * @date 18.04.2020
 */
@SpringBootTest
class ArticlesServiceImplTest(@Autowired private val articlesService: ArticlesService) {

    @Test
    fun `should throw ResponseStatusException for wrong slug`() {

        org.junit.jupiter.api.assertThrows<ResponseStatusException> { articlesService.getArticleBySlug("wrong-slug") }

    }
}
