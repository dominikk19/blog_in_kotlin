package com.example.blog

interface UserService {
    fun findUserByLogin(login : String) : UserDto
}
