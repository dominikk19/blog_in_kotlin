package com.example.blog

import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * @author Dominik Kiszka {dominikk19}
 * @project blog
 * @date 18.04.2020
 */
@Configuration
class BlogConfiguration {

    @Bean
    fun databaseInitializer(userRepository: UserRepository, articleRepository: ArticleRepository) = ApplicationRunner{
        val userTom = User("tomB", "Tom", "Big", "form Poland")
        userRepository.save(userTom)
        articleRepository.save(Article("Spring application with Kotlin",
                "Spring community ...",
                "Lorem ipsum",
                userTom))

        articleRepository.save(Article(
                title = "Kotlin Power",
                headLine = "Lorem ipsum",
                content = "dolor sit amet",
                author = userTom
        ))
    }
}
