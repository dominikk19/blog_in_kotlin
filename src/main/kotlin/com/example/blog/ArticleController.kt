package com.example.blog

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * @author Dominik Kiszka {dominikk19}
 * @project blog
 * @date 18.04.2020
 */
@RestController
@RequestMapping("/articles")
class ArticleController(private val service: ArticlesService) {


    @GetMapping("")
    fun getAllArticles() :ResponseEntity<List<ArticleDto>> = ResponseEntity.ok(service.getAllArticles())


    @GetMapping("/{slug}")
    fun getArticleBySlug(@PathVariable slug : String) = ResponseEntity.ok(service.getArticleBySlug(slug))
}

@RestController
@RequestMapping("/users")
class UserController(private val service : UserService){

    @GetMapping("/{login}")
    fun findUserByLogin(@PathVariable login : String) = ResponseEntity.ok(service.findUserByLogin(login))

}

