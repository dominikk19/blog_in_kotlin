package com.example.blog

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException


@Service
class ArticlesServiceImpl(private val repository: ArticleRepository) : ArticlesService {

    override fun getAllArticles(): List<ArticleDto> = repository.findAllByOrderByAddedAtDesc()
            .map { article -> mapArticleToDto(article) }
            .toCollection(destination = mutableListOf())

    override fun getArticleBySlug(slug: String): ArticleDto = repository.findBySlug(slug)
            ?.let { mapArticleToDto(it) }?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "This article does not exist")

}
