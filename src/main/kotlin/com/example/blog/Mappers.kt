package com.example.blog

/**
 * @author Dominik Kiszka {dominikk19}
 * @project blog
 * @date 18.04.2020
 */


fun mapArticleToDto(article: Article): ArticleDto {
    return ArticleDto(article.title,
            article.headLine,
            article.content,
            article.author,
            article.slug,
            article.addedAt,
            article.id!!
    )
}


fun User.mapToDto() : UserDto = UserDto(login,description)

