package com.example.blog

import java.time.LocalDateTime

class ArticleDto(val title: String,
                 val headLine: String,
                 val content: String,
                 val author: User,
                 val slug: String,
                 val addedAt: LocalDateTime,
                 val id: Long)


class UserDto(
        val login: String,
        val description: String?)
