package com.example.blog

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * @author Dominik Kiszka {dominikk19}
 * @project blog
 * @date 17.04.2020
 */
fun LocalDateTime.format() = this.format(DateTimeFormatter.ISO_LOCAL_DATE)

fun String.toSlug() = toLowerCase()
        .replace("\n", "")
        .replace("[^a-z\\d\\s]".toRegex(), " ")
        .split(" ")
        .joinToString("-")
        .replace("-+".toRegex(), "-")
