package com.example.blog

import org.springframework.data.repository.CrudRepository
import java.util.*

/**
 * @author Dominik Kiszka {dominikk19}
 * @project blog
 * @date 17.04.2020
 */
interface ArticleRepository : CrudRepository<Article, Long> {
    override fun findById(id: Long): Optional<Article>
    fun findAllByOrderByAddedAtDesc(): List<Article>
    fun findBySlug(slug: String): Article?
}

interface UserRepository : CrudRepository<User, Long> {
    fun findByLogin(login: String): User?
}


