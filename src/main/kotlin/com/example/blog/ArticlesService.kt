package com.example.blog

/**
 * @author Dominik Kiszka {dominikk19}
 * @project blog
 * @date 18.04.2020
 */
interface ArticlesService {
    fun getAllArticles(): List<ArticleDto>
    fun getArticleBySlug(slug: String): ArticleDto
}
