package com.example.blog

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class UserServiceImpl(private val userRepository: UserRepository) : UserService {

    override fun findUserByLogin(login: String) : UserDto = userRepository.findByLogin(login)?.mapToDto()?:throw ResponseStatusException(HttpStatus.NOT_FOUND,"User does not exist")

}
